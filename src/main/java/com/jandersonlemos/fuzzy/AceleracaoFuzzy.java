package com.jandersonlemos.fuzzy;

import net.sourceforge.jFuzzyLogic.FIS;

public class AceleracaoFuzzy {
	private Double distancia;
	private Double velocidade_via;
	private Double velocidade;
	private FIS fis;
	private String fileName;

	public AceleracaoFuzzy() {

	}

	/**
	 * 
	 * @param filenameFCL
	 * @param distancia
	 * @param velocidade_via
	 * @throws Exception
	 */

	public AceleracaoFuzzy(String filenameFCL, Double distancia, Double velocidade_via) throws Exception {
		this.setFileName(filenameFCL);
		this.setDistancia(distancia);
		this.setVelocidade_via(velocidade_via);
		this.init();
	}

	/**
	 * Carrega o arquivo FCL
	 * 
	 * @throws Exception
	 */
	public void init() throws Exception {
		this.fis = FIS.load(fileName, true);
		if (fis == null) {
			System.err.println("Can't load file: '" + fileName + "'");
			return;
		}
	}

	/**
	 * Executa a avaliação dos parametros
	 */
	public void evaluate() {

		fis.setVariable("velocidade_via", this.velocidade_via);
		fis.setVariable("distancia", this.distancia);
		this.fis.evaluate();
		this.setVelocidade(this.fis.getVariable("velocidade").defuzzify());
	}

	/**
	 * retorna da distancia definida para o veículo da frente
	 * 
	 * @return distancia
	 */

	public Double getDistancia() {
		return distancia;
	}

	/**
	 * Define o valor da distancia para o veículo da frente
	 * 
	 * @param distancia
	 */

	public void setDistancia(Double distancia) {
		this.distancia = distancia;
	}

	/**
	 * Retorna o valor definido para a velocidade da via
	 * 
	 * @return
	 */
	public Double getVelocidade_via() {
		return velocidade_via;
	}

	/**
	 * Define o valor da velocidade da via
	 * 
	 * @param velocidade_via
	 */
	public void setVelocidade_via(Double velocidade_via) {
		this.velocidade_via = velocidade_via;
	}

	/**
	 * Retorna o caminho do arquivo FCL definido
	 * 
	 * @return
	 */

	public String getFileName() {
		return fileName;
	}

	/**
	 * Define o caminho do arquivo FCL
	 * 
	 * @param fileName
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * Retorna a velocidade segura avaliada
	 * 
	 * @return
	 */
	public Double getVelocidade() {
		return velocidade;
	}

	/**
	 * Define a velocidade segura avaliada
	 * 
	 * @param velocidade
	 */

	private void setVelocidade(Double velocidade) {
		this.velocidade = velocidade;
	}

	/**
	 * Obtem o resumo dos valores definidos e avaliados.
	 */
	@Override
	public String toString() {

		Double velocidade = this.getVelocidade();
		StringBuilder sb = new StringBuilder();
		// System.out.println(fis);
		sb.append("Distancia (m): " + this.fis.getVariable("distancia").getValue());
		sb.append("\n");
		sb.append("Velocidade da via (km/h): " + this.fis.getVariable("velocidade_via").getValue());
		sb.append("\n");
		sb.append("Percentual: " + this.fis.getVariable("velocidade").getValue());
		sb.append("\n");
		sb.append(Math.round(velocidade) + "% da Via ->"
				+ Math.round((velocidade * this.fis.getVariable("velocidade_via").getValue() / 100)) + "km/h ");
		sb.append("\n\n");
		return sb.toString();
	}

	public static void main(String[] args) {

		try {
			// Instancia e define os valores para avaliação
			// o arquivo .fcl contém as regras fuzificação, termos linguisticos e regras para defuzificação 
			AceleracaoFuzzy af1 = new AceleracaoFuzzy("fcl/aceleracao.fcl", 40D, 80D);
			// Executa a avaliação
			af1.evaluate();
			// Exibe os resultados obtidos
			System.out.println(af1.toString());

			// Instancia e define os valores para avaliação
			AceleracaoFuzzy af2 = new AceleracaoFuzzy("fcl/aceleracao.fcl", 17D, 60D);
			// Executa a avaliação
			af2.evaluate();
			// Exibe os resultados obtidos
			System.out.println(af2.toString());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
