# Controle de aceleração com lógica Fuzzy

Controla a aceleração a partir da velocidade da via e a distancia do veículo da frente


## Instalação e Execução

Pela IDE Eclipse execute a opção Run As -> Maven Install e depois execute o arquivo AceleracaoFuzzy.java

Utilizando a lib [JFuzzyLogic](http://jfuzzylogic.sourceforge.net/html/manual.html#membership)


### Observações importantes

O arquivo que está fcl/aceleracao.fcl contém as regras fuzificação, termos linguisticos e regras para defuzificação 